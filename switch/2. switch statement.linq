<Query Kind="Statements" />

// switch statements may result in cleaner code than multiple if statements:

ShowCard(5);
ShowCard(11);
ShowCard(13);

static void ShowCard(int cardNumber)
{
Queen: if (cardNumber == 13)
	{
		Console.WriteLine("King");
	}
	else if (cardNumber == 12)
	{
		Console.WriteLine("Queen");
	}
	else if (cardNumber == 11)
	{
		Console.WriteLine("Jack");
	}
	else if (cardNumber == -1)  // Joker is -1.
	{
		cardNumber = 12;
		goto Queen;
	}
	else
	{
		Console.WriteLine(cardNumber);
	}
}

static void ShowCard1(int cardNumber)
{
	switch (cardNumber)
	{
		case 13:
			Console.WriteLine("King");
			break;
		case 12:
			Console.WriteLine("Queen");
			break;
		case 11:
			Console.WriteLine("Jack");
			break;
		case -1:                            // Joker is -1.
			goto case 12;                   // In this game joker counts as queen.
		default:                            // Executes for any other cardNumber.
			Console.WriteLine(cardNumber);
			break;
	}
}