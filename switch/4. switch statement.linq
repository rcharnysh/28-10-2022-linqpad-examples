<Query Kind="Statements" />

ShowCard(11);
ShowCard(12);
ShowCard(13);
ShowCard(10);

void ShowCard1(int cardNumber)
{
	if (cardNumber == 11 || cardNumber == 12 || cardNumber == 13)
	{
		Console.WriteLine("Face card");
	}
	else
	{
		Console.WriteLine("Plain card");
	}
}

void ShowCard(int cardNumber)
{
	if (cardNumber is 11 or 12 or 13)
	{
		Console.WriteLine("Face card");
	}
	else
	{
		Console.WriteLine("Plain card");
	}
}

void ShowCard2(int cardNumber)//Stacking Cases
{
	switch (cardNumber)
	{
		case 13:
		case 12:
		case 11:
			Console.WriteLine("Face card"); 
			break;
		default:
			Console.WriteLine("Plain card");
			break;
	}
}