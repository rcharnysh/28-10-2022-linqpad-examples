<Query Kind="Statements" />

#if false
for (int number = 0; number < 10; number++)
{
	Console.Write(number);
}
#endif

#if false
for (int number = 0; number < 10; number++)
{
	Console.Write($"{number}, ");
}
#endif

#if false
for (int number = 1; number <= 10; number++)
{
	string separator = number == 10 ? string.Empty : ", ";// "" == string.Empty
	Console.Write($"{number}{separator}");
}
#endif

#if false
int count = 10;
for (int number = 1; number <= 100; number++)
{
	string separator = (number % count) switch
	{
		0 when number == 100 => string.Empty,
		0 => $", {Environment.NewLine}",// Environment.NewLine - Gets the newline string defined for this environment.
		_ => number == 100 ? string.Empty : ", ",
	};

	Console.Write($"{number}{separator}");
}
#endif

#if false
int count = int.Parse(Console.ReadLine());
int borderValue = int.Parse(Console.ReadLine());

for (int number = 1; number <= borderValue; number++)
{
	string separator = (number % count) switch
	{
		0 when number == borderValue => string.Empty,
		0 => $", {Environment.NewLine}",// Environment.NewLine - Gets the newline string defined for this environment.
		_ => number == borderValue ? string.Empty : ", ",
	};

	Console.Write($"{number}{separator}");
}
#endif

#if true

int borderValue;

while (true)
{
	Console.Write("Enter border value > 0: ");
	borderValue = int.Parse(Console.ReadLine());
	if (borderValue > 0)
	{
		break;
	}

	Console.WriteLine("Invalid value. Try again.");
}

int count;

while (true)
{
	Console.Write("Enter count value > 0: ");
	count = int.Parse(Console.ReadLine());
	if (count > 0)
	{
		break;
	}

	Console.WriteLine("Invalid value. Try again.");
}

Console.WriteLine();

for (int number = 1; number <= borderValue; number++)
{
	string separator = (number % count) switch
	{
		0 when number == bourderValue => string.Empty,
		0 => $", {Environment.NewLine}",// Environment.NewLine - Gets the newline string defined for this environment.
		_ => number == bourderValue ? string.Empty : ", ",
	};

	Console.Write($"{number}{separator}");
}
#endif

#if false

char @continue = 'y';
int number = 1;
do
{
	Console.WriteLine(number++);
	Console.WriteLine("Press 'y' or 'Y' if want to continue.");
	@continue = char.Parse(Console.ReadLine());
	//Console.Clear();
} while (@continue is 'y' or 'Y');
//----------------------
Console.WriteLine("Finished!");

#endif

#if false
int count, borderValue;

while (true)
{
	Console.Write("Enter border value > 0: ");
	borderValue = int.Parse(Console.ReadLine());
	//Console.Clear();
	if (borderValue > 0)
	{
		break;
	}

	Console.WriteLine("Invalid value. Try again.");
}

while (true)
{
	Console.Write("Enter border value > 0: ");
	count = int.Parse(Console.ReadLine());
	//Console.Clear();
	if (count > 0)
	{
		break;
	}

	Console.WriteLine("Invalid value. Try again.");
}

Console.WriteLine();

for (int number = 1; number <= borderValue; number++)
{
	string end = (number % count) switch
	{
		0 when number == borderValue => String.Empty,
		0 => Environment.NewLine,
		_ => ", "
	};

	Console.Write($"{number}{end}");
}
#endif

#if false

char @continue = 'y';

do
{
	int count, borderValue;

	while (true)
	{
		Console.Write("Enter border value > 0: ");
		borderValue = int.Parse(Console.ReadLine());
		//Console.Clear();
		if (borderValue > 0)
		{
			break;
		}

		Console.WriteLine("Invalid value. Try again.");
	}

	while (true)
	{
		Console.Write("Enter border value > 0: ");
		count = int.Parse(Console.ReadLine());
		//Console.Clear();
		if (count > 0)
		{
			break;
		}

		Console.WriteLine("Invalid value. Try again.");
	}

	Console.WriteLine();

	const string fizz = "Fizz";
	const string buzz = "Buzz";

	for (int number = 1; number <= borderValue; number++)
	{
		string value = (number % 3) switch
		{
			0 when number % 5 == 0 => $"{fizz}{buzz}",
			0 => fizz,
			_ => (number % 5) == 0 ? buzz : $"{number}",
		};
		
		string end = (number % count) switch
		{
			0 when i == borderValue => String.Empty,
			0 => Environment.NewLine,
			_ => ", "
		};

		Console.Write($"{value}{end}");
	}
	Console.WriteLine();
	Console.WriteLine("Press 'y' or 'Y' if want to continue.");
	@continue = char.Parse(Console.ReadLine());
	//Console.Clear();
} while (@continue is 'y' or 'Y');
//----------------------
Console.WriteLine("Finished!");

#endif