<Query Kind="Statements" />

int x = 0;

while (true)
{
	Console.Write("Enter number with value > 0:");
	x = int.Parse(Console.ReadLine());
	if (x > 0)
	{
		break;
	}
	Console.WriteLine("Invalid value. Try again.");
}

Console.WriteLine($"x = {x}");